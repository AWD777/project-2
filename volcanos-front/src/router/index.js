import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "volcano" */ '../views/VolcanoHomeView.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: () => import(/* webpackChunkName: "detail" */ '../views/VolcanoDetailView.vue')
  },
  {
    path: '/newvolcano/:save/:id',
    name: 'newvolcano',
    component: () => import(/* webpackChunkName: "newvolcano" */ '../components/VolcanoNewView.vue')
  },
  {
    path: '/newstatus/:save/:id',
    name: 'newstatus',
    component: () => import(/* webpackChunkName: "newstatus" */ '../components/StatusNewView.vue')
  },
  {
    path: '/status',
    name: 'status',
    component: () => import(/* webpackChunkName: "status" */ '../views/StatusView.vue')
  },
  {
    path: '/editprofile/:id/:save/',
    name: 'editprofile',
    component: () => import(/* webpackChunkName: "editprofile" */ '../components/ProfileVolcanoNewView.vue')
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
