<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function(){
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate_otp_code');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('update-account','UpdateAccountController')->name('auth.update_account');
    Route::post('login', 'LoginController')->name('auth.login');
});

Route::apiResource('state', 'StatusesController');
// Route::get('status', 'StatusesController@index');
// Route::post('status', 'StatusesController@store');
// Route::get('status/{id}', 'StatusesController@show');
// Route::post('status/{id}', 'StatusesController@show');

Route::get('stateinfo/liststatus', 'StatusesController@liststatus');
Route::get('volcano/all', 'VolcanosController@indexall');
Route::get('volcano/profile/{id}', 'VolcanosController@showdetail');

Route::apiResource('volcano', 'VolcanosController');
Route::apiResource('profile', 'ProfilesController');
