<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use App\Events\RegenerateOtpCodeEvent;
use App\Mail\RegenerateOtpCodeMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToRegenerateOtpCodeUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateOtpCodeEvent  $event
     * @return void
     */
    public function handle(RegenerateOtpCodeEvent $event)
    {
        Mail::to($event->otp_code->user->email)->send(new RegenerateOtpCodeMail($event->otp_code));
    }
}
