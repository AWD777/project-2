<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Volcanos extends Model
{
    protected $table = 'volcanos';
    protected $fillable = ['nama', 'statuses_id', 'sejarah'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function statuses()
    {
        return $this->belongsTo('App\Statuses');
    }

    public function profiles()
    {
        return $this->hasOne('App\Profiles');
    }
}
