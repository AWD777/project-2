<?php

namespace App\Http\Controllers;

use App\Profiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File; 

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profiles::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data profile',
            'data'    => $profile
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'alamat' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'elevation' => 'integer',
            'foto'=>'image|mimes:jpeg,jpg,png,gif|max:2048',
            'volcanos_id'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        try {
            $data = $request->all();
            if($request->has('foto'))
            {
                $posterFileName = time() . "." . $request->foto->extension();
                $request->file('foto')->move('img', $posterFileName);
                $data['foto'] = $posterFileName;
            }
            
            $profile = Profiles::create($data);

            if ($profile) {
                return response()->json([
                    'success' => true,
                    'message' => 'profile Created',
                    'data'    => $profile
                ], 201);
            }
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'profile Failed to Save',
            ], 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profiles::findOrfail($id);

        return response()->json([
            'success' => true,
            'message' => 'Detail Data profile',
            'data'    => $profile
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'alamat'=>'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'elevation'=>'integer',
            'foto'=>'image|mimes:jpeg,jpg,png,gif|max:2048',
            'volcanos_id'=>'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $profile = Profiles::findOrFail($id);

        try {
            if ($profile) {
                if($request->has('foto'))
                {
                    $path='img/';
                    File::delete($path.$profile->foto);

                    $posterFileName = time() . "." . $request->foto->extension();
                    $request->file('foto')->move('img', $posterFileName);
                }else{
                    $posterFileName=$profile['foto'];
                }

                $profile->update([
                    'alamat'=>$request->alamat,
                    'latitude'=>$request->latitude,
                    'longitude'=>$request->longitude,
                    'elevation'=>$request->elevation,
                    'foto'=>$posterFileName,
                    'volcanos_id'=>$request->volcanos_id
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'profile Updated',
                    'data'    => $profile
                ], 200);
            }
        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => 'profile Not Found',
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $profile = Profiles::findOrfail($id);

            if ($profile) {
                $path='img/';
                File::delete($path.$profile->foto);
                $profile->delete();

                return response()->json([
                    'success' => true,
                    'message' => 'profile Deleted',
                ], 200);
            }
        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => 'profile Not Found',
            ], 404);
        }
    }
}
