<?php

namespace App\Http\Controllers;

use App\Statuses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = Statuses::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Status',
            'data'    => $status,
        ], 200);
    }



    public function liststatus()
    {
        $status = Statuses::all();

        $simple_array = array(); //simple array
        foreach($status as $d)
        {
                $simple_array[]=$d['id'].'||'.$d['status'];   
        }     
        return response()->json([
            'success' => true,
            'message' => 'List Data Status',
            'data'    => $simple_array,
        ], 200);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status'   => 'required',
            'info' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        try {
            $status = Statuses::create($request->all());
            if ($status) {
                return response()->json([
                    'success' => true,
                    'message' => 'status Created',
                    'data'    => $status
                ], 201);
            }
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'status Failed to Save',
            ], 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = Statuses::findOrfail($id);

        return response()->json([
            'success' => true,
            'message' => 'Detail Data status',
            'data'    => $status,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status'   => 'required',
            'info' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $status = Statuses::findOrFail($id);

        try {

            if ($status) {
                $status->update([
                    'status'     => $request->status,
                    'info'   => $request->info
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'status Updated',
                    'data'    => $status
                ], 200);
            }
        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => 'status Not Found',
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $status = Statuses::findOrfail($id);

            if ($status) {
                $status->delete();

                return response()->json([
                    'success' => true,
                    'message' => 'status Deleted',
                ], 200);
            }
        } catch (\Exception $e) {
            
            return response()->json([
                'success' => false,
                'message' => 'status Not Found',
            ], 404);
        }
    }
}
