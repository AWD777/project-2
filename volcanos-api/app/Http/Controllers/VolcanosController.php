<?php

namespace App\Http\Controllers;

use App\Profiles;
use App\Statuses;
use App\Volcanos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VolcanosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $volcano = Volcanos::inRandomOrder()->limit(4)->get();
 
        return response()->json([
            'success' => true,
            'message' => 'List 4 Random Data volcano',
            'data'    => $volcano
        ], 200);
    }

    public function indexall()
    {
        $volcano = Volcanos::latest()->get();
 
        return response()->json([
            'success' => true,
            'message' => 'List All Data volcano',
            'data'    => $volcano
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama'   => 'required',
            'statuses_id' => 'required',
            'sejarah'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        try {
            $volcano = Volcanos::create($request->all());
            if ($volcano) {
                return response()->json([
                    'success' => true,
                    'message' => 'volcano Created',
                    'data'    => $volcano
                ], 201);
            }
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'volcano Failed to Save',
            ], 409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $volcano = Volcanos::findOrfail($id);

        return response()->json([
            'success' => true,
            'message' => 'Detail Data volcano',
            'data'    => $volcano
        ], 200);
    }

    public function showdetail($id)
    {
        $volcano = Volcanos::findOrfail($id);
        $profile = Profiles::where('volcanos_id', $id)->first();
        $status = Statuses::where('id', $volcano['statuses_id'])->first();

        if($profile){
            return response()->json([
                'success' => true,
                'message' => 'Detail Data volcano',
                'data'    => [
                    'id'=>$volcano['id'],
                    'nama'=>$volcano['nama'],
                    'status'=>$status['status'],
                    'sejarah'=>$volcano['sejarah'],
                    'alamat'=>$profile['alamat'],
                    'latitude'=>$profile['latitude'],
                    'longitude'=>$profile['longitude'],
                    'elevation'=>$profile['elevation'],
                    'foto'=>$profile['foto'],
                    'profile_id'=>$profile['id'],
                    ]
            ], 200);
        }else{
            return response()->json([
                'success' => true,
                'message' => 'Detail Data volcano',
                'data'    => [
                    'id'=>$volcano['id'],
                    'nama'=>$volcano['nama'],
                    'status'=>$status['status'],
                    'sejarah'=>$volcano['sejarah'],
                    'alamat'=>'',
                    'latitude'=>'',
                    'longitude'=>'',
                    'elevation'=>'',
                    'foto'=>null,
                    'profile_id'=>null,
                    ]
            ], 200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama'   => 'required',
            'statuses_id' => 'required',
            'sejarah'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $volcano = Volcanos::findOrFail($id);

        try {

            if ($volcano) {
                $volcano->update([
                    'nama'   => $request->nama,
                    'statuses_id' => $request->statuses_id,
                    'sejarah'=>$request->sejarah,
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'volcano Updated',
                    'data'    => $volcano
                ], 200);
            }
        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => 'volcano Not Found',
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $volcano = Volcanos::findOrfail($id);

            if ($volcano) {
                $volcano->delete();

                return response()->json([
                    'success' => true,
                    'message' => 'volcano Deleted',
                ], 200);
            }
        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => 'volcano Not Found',
            ], 404);
        }
    }
}
