<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\UserRegisterEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
         //set validation
         $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'email' => 'required|unique:users,email|email',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create($request->all());

        do {
            $random = mt_rand(100000,999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);
   
        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(60),
            'user_id' => $user->id
        ]);

         //memanggil event UserRegisterEvent
        event(new UserRegisterEvent($otp_code));

        return response()->json([
                'success' => true,
                'message' => 'Data User berhasil dibuat',
                'data' => [
                    'user' => $user,
                    'otp_code' => $otp_code
                ]
            ]);
        
    }
}
