<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdateAccountController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name'     => 'required',
            'email'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user)
        {
            return response()->json([
                'success' => false,
                'message' => 'Email tidak ditemukan'
            ], 400);
        }

        $user->update([
            'name'     => $request->name,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Nama berhasil diupdate',
            'data' => $user
        ]);
    }
}
