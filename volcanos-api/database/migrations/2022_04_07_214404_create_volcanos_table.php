<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVolcanosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volcanos', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('nama',100);
            $table->uuid('statuses_id');
            $table->foreign('statuses_id')->references('id')->on('statuses');
            $table->text('sejarah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volcanos');
    }
}
