<h1><b>Final Project 2</b></h1>

## Grup Diskusi 2 - Kelompok 2

## Anggota Kelompok

- Anggara Wahyu Dwiatmaja  @AWD_777
- Ahmad Ali Fahmi    @alifahmi

## Tema Project

Database Gunung Api di Indonesia

## ERD

![project_2.png](./image/project 2.png)

## Dokumentasi API
- https://documenter.getpostman.com/view/20071582/UVyyrs7k
- https://documenter.getpostman.com/view/6660221/UVyyrs3J

## Screenshot

![screenshot1.png](./image/screenshot1.png)
![screenshot2a.png](./image/screenshot2a.png)
![screenshot3.png](./image/screenshot3.png)
![screenshot4.png](./image/screenshot4.png)
![screenshot5.png](./image/screenshot5.png)
![screenshot6.png](./image/screenshot6.png)
![screenshot7.png](./image/screenshot7.png)
![screenshot8.png](./image/screenshot8.png)

## Link Live Demo

https://youtu.be/Jr3MF9NCM1g